package com.example.likelionsns2.domain.entity;

import com.example.likelionsns2.domain.dto.post.PostDto;
import com.example.likelionsns2.domain.dto.post.PostUpdateRequest;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

import static javax.persistence.FetchType.LAZY;

@Entity
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Post {

    @Id @GeneratedValue
    @Column(name = "post_id")
    private Long id;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private User user;

    private String title;
    private String body;

    private LocalDateTime registeredAt;
    private LocalDateTime updateAt;
    private LocalDateTime deletedAt;


    /**
     * Post 수정 메서드
     */
    public Post modify(PostUpdateRequest request) {
        setTitle(request.getTitle());
        setBody(request.getBody());
        setUpdateAt(LocalDateTime.now());
        return this;
    }

    /**
     * DTO로 변환
     */
    public PostDto toDto() {
        return PostDto.builder()
                .id(this.id)
                .user(this.user)
                .title(this.title)
                .body(this.body)
                .registeredAt(this.registeredAt)
                .updateAt(this.updateAt)
                .deletedAt(this.deletedAt)
                .build();
    }

    /**
     * Setter
     */
    private void setTitle(String title) {
        this.title = title;
    }

    private void setBody(String body) {
        this.body = body;
    }

    private void setUpdateAt(LocalDateTime updateAt) {
        this.updateAt = updateAt;
    }
}
