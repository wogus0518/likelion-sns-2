package com.example.likelionsns2.domain.dto.user;

import com.example.likelionsns2.domain.UserRole;
import com.example.likelionsns2.domain.entity.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.time.LocalDateTime;

@Getter
@ToString
@AllArgsConstructor
public class UserJoinRequest {
    private final String userName;
    private final String password;
    private final UserRole role;

    public User toEntity(String password) {
        return User.builder()
                .userName(this.userName)
                .password(password)
                .role(this.role)
                .registeredAt(LocalDateTime.now())
                .build();
    }

}
