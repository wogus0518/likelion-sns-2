package com.example.likelionsns2.domain.dto.post;

import com.example.likelionsns2.domain.entity.Post;
import com.example.likelionsns2.domain.entity.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.time.LocalDateTime;

@Getter
@ToString
@AllArgsConstructor
public class PostCreateRequest {
    private final String title;
    private final String body;

    public Post toEntity(User user) {
        return Post.builder()
                .user(user)
                .title(this.title)
                .body(this.body)
                .registeredAt(LocalDateTime.now())
                .build();
    }
}
