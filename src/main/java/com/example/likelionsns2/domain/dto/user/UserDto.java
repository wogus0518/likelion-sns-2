package com.example.likelionsns2.domain.dto.user;

import com.example.likelionsns2.domain.UserRole;
import lombok.*;

@Getter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
    private Long id;
    private String userName;
    private String password;
    private UserRole role;
}
