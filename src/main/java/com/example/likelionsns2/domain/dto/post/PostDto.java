package com.example.likelionsns2.domain.dto.post;

import com.example.likelionsns2.domain.entity.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PostDto {
    private Long id;
    private User user;
    private String title;
    private String body;
    private LocalDateTime registeredAt;
    private LocalDateTime updateAt;
    private LocalDateTime deletedAt;
}
