package com.example.likelionsns2.domain.dto.user;

import com.example.likelionsns2.domain.UserRole;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class UserJoinResponse {
    private final Long userId;
    private final String userName;
    private final UserRole role;
}
