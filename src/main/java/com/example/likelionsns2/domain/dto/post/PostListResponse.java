package com.example.likelionsns2.domain.dto.post;

import com.example.likelionsns2.domain.entity.Post;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.data.domain.Page;

@Getter
@AllArgsConstructor
public class PostListResponse {
    private final Page<Post> posts;
}
