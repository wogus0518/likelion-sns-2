package com.example.likelionsns2.domain;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum UserRole {
    ROLE_ADMIN, ROLE_USER;

    @JsonCreator
    public static UserRole from(String s) {
        return UserRole.valueOf(s.toUpperCase());
    }
}
