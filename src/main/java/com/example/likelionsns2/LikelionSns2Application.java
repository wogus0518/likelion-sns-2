package com.example.likelionsns2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LikelionSns2Application {

    public static void main(String[] args) {
        SpringApplication.run(LikelionSns2Application.class, args);
    }

}
