package com.example.likelionsns2.controller;

import com.example.likelionsns2.domain.Response;
import com.example.likelionsns2.domain.dto.post.*;
import com.example.likelionsns2.domain.entity.Post;
import com.example.likelionsns2.exception.ErrorCode;
import com.example.likelionsns2.exception.SnsAppException;
import com.example.likelionsns2.service.PostService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/posts")
public class PostController {

    private final PostService postService;

    /**
     * post 전체 조회
     */
    @GetMapping
    public Response<PostListResponse> getPostList(Pageable pageable) {
        Page<Post> posts = postService.findAll(pageable);
        if (posts.getNumberOfElements() > 0) {
            return Response.success(new PostListResponse(posts));
        } else {
            throw new SnsAppException(ErrorCode.POST_NOT_FOUND);
        }
    }

    /**
     * post 등록
     * 로그인 인증이 된 사람에 한해서만
     */
    @PostMapping
    public Response<PostCreateResponse> createPost(Authentication auth,
                                                   @RequestBody PostCreateRequest postCreateRequest) {
        String userName = auth.getName();

        PostDto postDto = postService.savePost(postCreateRequest, userName);

        return Response.success(new PostCreateResponse(postDto.getId(), "포스트 등록 완료"));
    }

    /**
     * 포스트 상세 조회
     */
    @GetMapping("/{postId}")
    public Response<PostOneResponse> getPostOne(@PathVariable Long postId) {
        PostDto post = postService.findById(postId);
        log.info("user={}", post.getUser().toString());
        PostOneResponse response = new PostOneResponse(post.getId(), post.getTitle(), post.getBody(),
                post.getUser().getUsername(), post.getRegisteredAt(), post.getUpdateAt());

        return Response.success(response);
    }

    /**
     * 포스트 수정
     */
    @PutMapping("/{postId}")
    public Response<PostCreateResponse> updatePost(@PathVariable Long postId,
                                                   @RequestBody PostUpdateRequest request,
                                                   Authentication auth) {
        PostDto postDto = postService.doUpdate(postId, request, auth);
        return Response.success(new PostCreateResponse(postDto.getId(), "포스트 수정 완료"));
    }

    /**
     * 포스트 삭제
     */
    @DeleteMapping("/{postId}")
    public Response<PostDeleteResponse> deletePost(@PathVariable Long postId,
                                                   Authentication auth) {
        Long deletedPostId = postService.doDelete(postId, auth);
        return Response.success(new PostDeleteResponse(deletedPostId));
    }

}
