package com.example.likelionsns2.service;

import com.example.likelionsns2.exception.ErrorCode;
import com.example.likelionsns2.exception.SnsAppException;
import com.example.likelionsns2.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;
    
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByUserName(username)
                .orElseThrow(() -> new SnsAppException(ErrorCode.USERNAME_NOT_FOUND));
    }
}
