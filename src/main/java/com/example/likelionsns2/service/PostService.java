package com.example.likelionsns2.service;

import com.example.likelionsns2.domain.UserRole;
import com.example.likelionsns2.domain.dto.post.PostCreateRequest;
import com.example.likelionsns2.domain.dto.post.PostDto;
import com.example.likelionsns2.domain.dto.post.PostUpdateRequest;
import com.example.likelionsns2.domain.entity.Post;
import com.example.likelionsns2.domain.entity.User;
import com.example.likelionsns2.exception.ErrorCode;
import com.example.likelionsns2.exception.SnsAppException;
import com.example.likelionsns2.repository.PostRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class PostService {

    private final PostRepository postRepository;
    private final UserService userService;

    @Transactional
    public PostDto savePost(PostCreateRequest postCreateRequest, String userName) {
        User user = userService.getUserByUserName(userName);
        Post savedPost = postRepository.save(postCreateRequest.toEntity(user));
        return savedPost.toDto();

    }

    public Page<Post> findAll(Pageable pageable) {
        return postRepository.findAll(pageable);
    }

    public PostDto findById(Long postId) {
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new SnsAppException(ErrorCode.POST_NOT_FOUND));
        return post.toDto();
    }

    @Transactional
    public PostDto doUpdate(Long postId, PostUpdateRequest request, Authentication auth) {
        //수정하려는 포스트가 존재하는지 postId로 체크
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new SnsAppException(ErrorCode.POST_NOT_FOUND));

        //수정하려는 포스트의 주인이거나 운영자면 업데이트 실시
        List<String> roles = auth.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());
        String userName = auth.getName();
        //수정 권한이 있다면 수정 실행
        if (hasPermission(post, roles, userName)) {
            Post updatedPost = post.modify(request);
            return updatedPost.toDto();
        } else {
            throw new SnsAppException(ErrorCode.INVALID_PERMISSION);
        }
    }

    @Transactional
    public Long doDelete(Long postId, Authentication auth) {
        //postId로 해당 포스트가 존재하는지 체크
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new SnsAppException(ErrorCode.POST_NOT_FOUND));

        if (post.getUser() == null) {
            throw new SnsAppException(ErrorCode.USERNAME_NOT_FOUND);
        }

        //삭제하려는 포스트 주인이거나 운영자면 삭제 실시
        List<String> roles = auth.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());        String userName = auth.getName();

        if (hasPermission(post, roles, userName)) {
            postRepository.delete(post);
            return post.getId();
        } else {
            throw new SnsAppException(ErrorCode.INVALID_PERMISSION);
        }
    }

    private static boolean hasPermission(Post post, List<String> roles, String userName) {
        return roles.contains(UserRole.ROLE_ADMIN.toString()) || post.getUser().getUsername().equals(userName);
    }
}
