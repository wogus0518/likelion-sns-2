package com.example.likelionsns2.service;

import com.example.likelionsns2.domain.dto.user.UserDto;
import com.example.likelionsns2.domain.dto.user.UserJoinRequest;
import com.example.likelionsns2.domain.dto.user.UserLoginRequest;
import com.example.likelionsns2.domain.entity.User;
import com.example.likelionsns2.exception.ErrorCode;
import com.example.likelionsns2.exception.SnsAppException;
import com.example.likelionsns2.repository.UserRepository;
import com.example.likelionsns2.security.utils.JwtTokenUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.example.likelionsns2.exception.ErrorCode.INVALID_PASSWORD;
import static com.example.likelionsns2.exception.ErrorCode.USERNAME_NOT_FOUND;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class UserService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder encoder;

    @Value("${jwt.token.secret}")
    private String secretKey;
    private long expireTimeMs = 1000 * 60 * 60;

    @Transactional
    public UserDto join(UserJoinRequest userJoinRequest) {

        //userName 중복 체크
        userRepository.findByUserName(userJoinRequest.getUserName())
                .ifPresent(user -> {
                    throw new SnsAppException(ErrorCode.DUPLICATED_LOGIN_ID);
                });

        //정상로직 - .save()
        User user = userRepository.save(userJoinRequest.toEntity(encoder.encode(userJoinRequest.getPassword())));
        return user.toDto();
    }

    public String login(UserLoginRequest userLoginRequest) {
        String token = "";
        String userName = userLoginRequest.getUserName();
        String password = userLoginRequest.getPassword();

        //userName 으로 검색해서 없으면 Not founded
        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new SnsAppException(USERNAME_NOT_FOUND));

        //password 일치 여부 확인
        boolean matches = encoder.matches(password, user.getPassword());
        if(!matches) throw new SnsAppException(INVALID_PASSWORD);

        //정상로직 => 로그인 실행
        token = JwtTokenUtil.createToken(userName, secretKey, expireTimeMs);
        return token;
    }

    public User getUserByUserName(String userName) {
        return userRepository.findByUserName(userName)
                .orElseThrow(() -> new SnsAppException(USERNAME_NOT_FOUND));
    }
}
