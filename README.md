# Like Lion SNS

- Gitlab
    - https://gitlab.com/wogus0518/likelion-sns-2
- Swagger
    - http://ec2-15-164-179-177.ap-northeast-2.compute.amazonaws.com:8082/swagger-ui/

---

## 수행 사항
- AWS EC2에 Docker 배포
- Gitlab CI & Crontab CD
- Swagger 문서 적용
- 유저 기능
  - 회원가입, 로그인
    - 회원 등급을 USER와 ADMIN으로 분리
    - Spring Security를 적용하여 로그인한 사용자에게 jwt 발급
    - 
- 포스트 기능
  - 등록, 수정, 삭제
    - 모두 인증된 사용자가 존재하는 경우에 기능이 작동
    - ADMIN의 요청이라면 포스트 작성자와 관계없이 기능이 작동
  - 조회
    - 로그인을 하지 않아도 조회는 가능

## 엔드포인트
- 엔드포인트는 Swagger를 이용하여 문서화 실시하였습니다.

## ERD
![image](https://user-images.githubusercontent.com/63176744/209636985-e655724e-05bc-433f-92b9-e851750594b4.png)

## 테스트 코드
- Controller
  - UserControllerTest
    - 회원가입 성공
    - 회원가입 실패 - userName 중복
    - 로그인 성공
    - 로그인 실패 - userName없음
    - 로그인 실패 - password틀림
  - PostControllerTest
    - 상세 조회 성공 - id, title, body, userName 4가지 항목이 있는지 검증
    - 포스트 작성 성공
    - 포스트 수정 실패(1) : 인증 실패
    - 포스트 수정 실패(2) : 작성자 불일치
    - 포스트 수정 실패(3) : 데이터베이스 에러
    - 포스트 수정 성공(1) : 작성자가 수정 요청
    - 포스트 수정 성공(2) : 관리자가 수정 요청
    - 포스트 삭제 성공(1) : 작성자가 삭제 요청
    - 포스트 삭제 성공(2) : 관리자가 삭제 요청
    - 포스트 수정 실패(1) : 인증 실패
    - 포스트 수정 실패(2) : 작성자 불일치
    - 포스트 수정 실패(3) : 데이터베이스 에러
  - Service
    - PostServiceTest
      - 등록 성공
      - 등록 실패 : 유저가 존재하지 않을 때
      - 수정 실패 : 포스트 존재하지 않음
      - 수정 실패 : 작성자!=유저
      - 수정 성공 : 작성자!=유저 이지만 ADMIN인 경우
      - 수정 성공 : 작성자 요청
      - 삭제 실패 : 포스트 존재하지 않음
      - 삭제 실패 : 작성자!=유저
      - 삭제 성공 : ADMIN인 경우